let express = require('express'),
    stripe = require('stripe')('sk_test_MKZR4fUwXkOPtt360LswDASw'),
    bodyParser = require('body-parser'),
    exphbs = require('express-handlebars'),
    app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(`${__dirname}/public`));

let port = process.env.PORT || 8080;

app.get('/', (req, res, next) => {
    res.render('index');
});

app.post('/charge', (req, res, next) => {
    const amount = 2500;

    stripe.customers.create({
        email: req.body.stripeEmail,
        source: req.body.stripeToken
    })
        .then(customer => stripe.charges.create({
        amount,
        description: 'Dummy item payment',
        currency: 'usd',
        customer: customer.id
    }))
        .then(charge => res.render('success'));
});

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});