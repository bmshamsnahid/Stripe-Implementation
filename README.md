# Stripe Payment Implementation

## Instructions

If you would like to download the code and try it for yourself:

1. Clone the repo: `git clone git@github.com:scotch-io/easy-node-authentication`
2. Install packages: `npm install`
3. Launch: `node server.js`
4. Visit in your browser at: `http://localhost:8080`
5. Now click the Pay With Card Button
6. Enter test card Number `4242 4242 4242 4242`
7. Date: `09/18`
8. CVC: `222`
9. Click the Pay button
10. Voila, You are done.